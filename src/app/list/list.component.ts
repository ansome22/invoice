import { Component, OnInit } from "@angular/core";
import { InvoiceService } from "../invoice.service";
import { Invoice } from "../invoice";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"]
})
export class ListComponent implements OnInit {
  invoices: Invoice[];
  constructor(private invoiceService: InvoiceService) {}

  ngOnInit() {
    this.getInvoices();
  }

  getInvoices(): void {
    this.invoiceService.getInvoices().subscribe(invoices => (this.invoices = invoices));
  }
}
