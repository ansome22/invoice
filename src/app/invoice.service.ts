import { Injectable } from "@angular/core";
import { Invoice } from "./invoice";
import { INVOICES } from "./mock-invoices";
import { Observable, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class InvoiceService {
  constructor() {}

  getInvoices(): Observable<Invoice[]> {
    return of(INVOICES);
  }

  getInvoice(id: number): Observable<Invoice> {
    return of(INVOICES.find(invoice => invoice.invoiceNumber === id));
  }
}
