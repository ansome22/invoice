export class Invoice {
  invoiceNumber: number;
  date: string;
  name: string;
  payable: Payable;
  billed: Billed;
  items: Items[];
  discount: number;
  account: string;
  reference: string;
}

export class Payable {
  name: string;
  person: string;
  address: string;
  city: string;
  zip: string;
  phone: string;
  email: string;
}

export class Billed {
  name: string;
  person: string;
  address: string;
  city: string;
  zip: string;
}

export class Items {
  id: number;
  title: string;
  price: number;
  quality: number;
}
