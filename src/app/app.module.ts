import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { ListComponent } from "./list/list.component";
import { UploadComponent } from "./upload/upload.component";
import { EditComponent } from "./edit/edit.component";
import { SearchComponent } from "./search/search.component";
import { ShowComponent } from "./show/show.component";
import { AddComponent } from './add/add.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListComponent,
    UploadComponent,
    EditComponent,
    SearchComponent,
    ShowComponent,
    AddComponent
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
