import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { InvoiceService } from "../invoice.service";
import { Invoice } from "../invoice";

@Component({
  selector: "app-show",
  templateUrl: "./show.component.html",
  styleUrls: ["./show.component.scss"]
})
export class ShowComponent implements OnInit {
  invoice: Invoice;
  myDate: string;
  myYear: string;
  myMonth: string;
  chartMonth: Chart;
  chartOverall: Chart;

  constructor(private route: ActivatedRoute, private invoiceService: InvoiceService) {}

  ngOnInit() {
    this.getInvoice();
  }

  getInvoice(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.invoiceService.getInvoice(id).subscribe(invoice => (this.invoice = invoice));
  }
}
