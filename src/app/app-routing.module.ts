import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { ListComponent } from "./list/list.component";
import { AddComponent } from "./add/add.component";
import { UploadComponent } from "./upload/upload.component";
import { EditComponent } from "./edit/edit.component";
import { ShowComponent } from "./show/show.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "list", component: ListComponent },
  { path: "show/:id", component: ShowComponent },
  { path: "add", component: AddComponent },
  { path: "upload", component: UploadComponent },
  { path: "edit", component: EditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
