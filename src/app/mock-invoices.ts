import { Invoice } from "./invoice";

export const INVOICES: Invoice[] = [
  {
    invoiceNumber: 1,
    name: "Mr. Nice",
    date: "16/01/2019",
    payable: {
      name: "Company",
      person: "Alex",
      address: "11 bames street",
      city: "Hamptin",
      zip: "2234",
      phone: "",
      email: "example@gmail.com"
    },
    billed: { name: "Company LTD", person: "Archer", address: "", city: "Hamptin", zip: "2234" },
    items: [
      { id: 1, title: "Item name", price: 20, quality: 1 },
      { id: 2, title: "Item name", price: 30, quality: 2 }
    ],
    discount: 0,
    account: "00-0000-0000000-00",
    reference: "Please quote client Surname and invoice number listed above"
  },
  {
    invoiceNumber: 2,
    name: "Mr. Nice",
    date: "5/03/2018",
    payable: {
      name: "Company",
      person: "James",
      address: "11 bames street",
      city: "Hamptin",
      zip: "2234",
      phone: "",
      email: "example@gmail.com"
    },
    billed: { name: "Company LTD", person: "Archer", address: "", city: "Hamptin", zip: "2234" },
    items: [{ id: 1, title: "Item name", price: 20, quality: 1 }],
    discount: 0,
    account: "00-0000-0000000-00",
    reference: "Please quote client Surname and invoice number listed above"
  },
  {
    invoiceNumber: 3,
    name: "Mr. Nice",
    date: "16/01/2019",
    payable: {
      name: "Company",
      person: "Alex",
      address: "11 bames street",
      city: "Hamptin",
      zip: "2234",
      phone: "",
      email: "example@gmail.com"
    },
    billed: { name: "Company LTD", person: "Archer", address: "", city: "Hamptin", zip: "2234" },
    items: [{ id: 1, title: "Item name", price: 20, quality: 1 }],
    discount: 0,
    account: "00-0000-0000000-00",
    reference: "Please quote client Surname and invoice number listed above"
  },
  {
    invoiceNumber: 4,
    name: "Mr. Nice",
    date: "10/02/2018",
    payable: {
      name: "Company",
      person: "James",
      address: "11 bames street",
      city: "Hamptin",
      zip: "2234",
      phone: "",
      email: "example@gmail.com"
    },
    billed: { name: "Company LTD", person: "Archer", address: "", city: "Hamptin", zip: "2234" },
    items: [{ id: 1, title: "Item name", price: 20, quality: 1 }],
    discount: 10,
    account: "00-0000-0000000-00",
    reference: "Please quote client Surname and invoice number listed above"
  },
  {
    invoiceNumber: 5,
    name: "Mr. Nice",
    date: "16/01/2019",
    payable: {
      name: "Company",
      person: "Alex",
      address: "11 bames street",
      city: "Hamptin",
      zip: "2234",
      phone: "",
      email: "example@gmail.com"
    },
    billed: { name: "Company LTD", person: "Archer", address: "", city: "Hamptin", zip: "2234" },
    items: [
      { id: 1, title: "Item name", price: 20, quality: 1 },
      { id: 2, title: "Item name", price: 30, quality: 2 }
    ],
    discount: 10,
    account: "00-0000-0000000-00",
    reference: "Please quote client Surname and invoice number listed above"
  },
  {
    invoiceNumber: 6,
    name: "Mr. Nice",
    date: "10/05/2018",
    payable: {
      name: "Company",
      person: "James",
      address: "11 bames street",
      city: "Hamptin",
      zip: "2234",
      phone: "",
      email: "example@gmail.com"
    },
    billed: { name: "Company LTD", person: "Archer", address: "", city: "Hamptin", zip: "2234" },
    items: [{ id: 1, title: "Item name", price: 20, quality: 1 }],
    discount: 0,
    account: "00-0000-0000000-00",
    reference: "Please quote client Surname and invoice number listed above"
  }
];
